# Grep with web interface #

This application is an analog of the system utility 'grep' on Linux.

Page for creating grep request.
![Page for grep request](https://bitbucket.org/happyanabolism/illustrations-for-projects/raw/master/grep_request.png)

Page with result.
![Page with result](https://bitbucket.org/happyanabolism/illustrations-for-projects/raw/master/grep_result.png)


Created on python/Django.
Database: sqlite3.

### What can this application do? ###

* search lines in files with matches by pattern (regular expression)
* search files that have matches with pattern
* count number of lines in files that have matches by pattern
* supported modes: lines with number mode, word expression mode, recursive searching files in folder

### How to install? ###

* python manage.py migrate

### How to run? ###

* python manage.py runserver

Created by Nikita Batura
mail: nik.batura.97@mail.ru