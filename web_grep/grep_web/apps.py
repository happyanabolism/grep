from django.apps import AppConfig


class GrepWebConfig(AppConfig):
    name = 'grep_web'
