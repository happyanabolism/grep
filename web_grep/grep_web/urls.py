from django.conf.urls import url

from views import GrepRequestCreate

urlpatterns = [
	url(r'^$', GrepRequestCreate.as_view(), name='index'),
]