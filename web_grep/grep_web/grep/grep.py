# -*- coding: utf-8 -*-

from re import findall, sub
from os import walk
from os.path import basename, exists, join, isdir

from exceptions2 import (DirectoryError, SourceError,
						 NotFoundError, RegexError)


def raise_dec(fn):
	"""Decorator for raising exceptions"""
	def wrapper(file_name, pattern, word_regexp, **kwargs):
		if not exists(file_name):
			raise SourceError(file_name)
		if isdir(file_name):
			raise DirectoryError(file_name)
		return fn(file_name, pattern, word_regexp, **kwargs)
	return wrapper


def recursive_file_search(file_name):
	"""Recursive searching path to file"""
	if not exists(file_name):
		for root, dirs, files in walk('.'):
			for file in files:
				if file == file_name:
					return join(root, file)
	return file_name


def recursive_files_search(directory):
	"""Return all files in a directory"""
	file_names = []
	for root, dirs, files in walk(directory):
		for file in files:
			file_names.append(join(root, file))
	return file_names


def get_files_with_lines(files, pattern, func,
				 word_regexp, line_number=False, ignore_errors=True):
	"""Return dictionary (key = file name, value = lines
	   in file with matches by pattern)"""
	dictionary = {}
	for file in files:
		try:
			directory[file] = []
		except:
			dictionary[basename(file)] = []

	for file in files:
		try:
			if func == get_count_of_matches:
				dictionary[basename(file)] = func(file, pattern,
													word_regexp)
			else:
				dictionary[basename(file)] = func(file, pattern, word_regexp,
								 line_number=line_number)
		except (SourceError, DirectoryError,
				RegexError, NotFoundError) as error:
			if ignore_errors:
				if func == get_count_of_matches:
					dictionary[basename(file)] = str(error)
				else:
					dictionary[basename(file)] = [str(error)]
			else:
				try:
					del dictionary[basename(file)]
				except:
					pass
	return dictionary


def get_files_with_match(files, pattern, func, word_regexp):
	"""Return files with match"""
	return get_files_with_lines(files, pattern, func,
				 word_regexp, ignore_errors=False).keys()


@raise_dec
def find_lines_with_match(file_name, pattern, word_regexp, line_number=False):
	"""Return lines in file without mathces by pattern"""
	if word_regexp:
		pattern = '{0}{1}{2}'.format('\\b', pattern, '\\b')

	lines, number = [], 0
	with open(file_name, 'r') as file:
		for line in file.readlines():
			number += 1
			matches = findall(pattern, line)
			if matches:
				if line_number:
					line = '{0}. {1}'.format(number, line)

				lines.append(line)
	if len(lines) == 0:
		raise NotFoundError(file_name)
	return lines


@raise_dec
def find_lines_with_non_match(file_name, pattern, word_regexp, line_number=False):
	"""Retturn lines in file without mathces by pattern"""
	if word_regexp:
		pattern = '{0}{1}{2}'.format('\\b', pattern, '\\b')

	lines, number = [], 0
	with open(file_name, 'r') as file:
		for line in file.readlines():
			number += 1
			if not findall(pattern, line):
				if line_number:
					line = '{0}. {1}'.format(number, line)

				lines.append(line)
	if len(lines) == 0:
		raise NotFoundError(file_name)
	return lines


@raise_dec
def get_count_of_matches(file_name, pattern, word_regexp):
	"""Return number of matches by pattern in file"""
	number = 0
	if word_regexp:
		pattern = '{0}{1}{2}'.format('\\b', pattern, '\\b')

	with open(file_name, 'r') as file:
		for line in file.readlines():
			number += len(findall(pattern, line))
	return number

