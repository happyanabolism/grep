# -*- coding: utf-8 -*-

class FileException(Exception):
	pass

class SourceError(FileException):
	def __str__(self):
		return 'no such file or directory' 

class DirectoryError(FileException):
	def __str__(self):
		return 'is a directory' 

class NotFoundError(FileException):
	def __str__(self):
		return 'not found with regexp' 

class RegexError(Exception):
	def __str__(self):
		return 'invalid regular expression' 