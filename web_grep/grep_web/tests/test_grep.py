# -*- coding: utf-8 -*-

import unittest

from grep import grep, exceptions2

class TestGrep(unittest.TestCase):
	def setUp(self):
		self.test_file = 'test_files/test_file'
		self.wrong_test_file = 'test_files/wrong_file'
		self.test_directory = 'test_files'
		self.pattern = 'test'

	def test_find_lines_with_match(self):
		test_lines = grep.find_lines_with_match(self.test_file,
						self.pattern, False)
		self.assertEqual(['test\n', 'testing\n',
					  	  'unit tests for application\n',
					      'module unittest\n'], test_lines)

		test_lines = grep.find_lines_with_match(self.test_file,
						self.pattern, True)
		self.assertEqual(['test\n'], test_lines)

		test_lines = grep.find_lines_with_match(self.test_file,
						self.pattern, True, line_number=True)
		self.assertEqual(['1. test\n'], test_lines)

	def test_find_lines_with_non_match(self):
		with self.assertRaises(exceptions2.NotFoundError):
			grep.find_lines_with_non_match(self.test_file,
			   self.pattern, False)

		test_lines = grep.find_lines_with_non_match(self.test_file,
						self.pattern, True)
		self.assertEqual(['testing\n',
					  	  'unit tests for application\n',
					      'module unittest\n'], test_lines)
		
		test_lines = grep.find_lines_with_non_match(self.test_file,
						self.pattern, True, line_number=True)
		self.assertEqual(['2. testing\n',
					  	  '3. unit tests for application\n',
					      '4. module unittest\n'], test_lines)

	def test_get_count_of_matches(self):
		test_count = grep.get_count_of_matches(self.test_file,
						self.pattern, False)
		self.assertEqual(4, test_count)

		test_count = grep.get_count_of_matches(self.test_file,
						self.pattern, True)
		self.assertEqual(1, test_count)

	def test_get_files_with_match(self):
		test_files = grep.get_files_with_match([self.test_file],
						self.pattern, grep.find_lines_with_match,
						False)
		self.assertEqual(['test_file'], test_files)

	def test_raising_errors(self):
		with self.assertRaises(exceptions2.SourceError):
			grep.find_lines_with_match(self.wrong_test_file,
			   self.pattern, False)
		
		with self.assertRaises(exceptions2.DirectoryError):
			grep.find_lines_with_non_match(self.test_directory,
				self.pattern, False)
		
		with self.assertRaises(exceptions2.SourceError):
			grep.get_count_of_matches(self.wrong_test_file,
				self.pattern, False)


if __name__ == '__main__':
	unittest.main()
