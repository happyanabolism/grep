from django.shortcuts import render, render_to_response
from django.views.generic.edit import CreateView

from models import GrepRequest
from forms import GrepRequestCreateForm
from grep import grep


class GrepRequestCreate(CreateView):
	model = GrepRequest
	template_name = 'grep_request.html'
	fields = ['action', 'files', 'pattern', 'count', 'invert',
				  'word_regexp', 'line_number', 'recursive']

	def get(self, request, *args, **kwargs):
		self.form = GrepRequestCreateForm()
		return super(GrepRequestCreate, self).get(request, *args, **kwargs)
	
	def get_context_data(self, **kwargs):
		context = super(GrepRequestCreate, self).get_context_data(**kwargs)
		context['form'] = self.form
		return context
	
	def post(self, request, *args, **kwargs):
		self.form = GrepRequestCreateForm(request.POST)
		if self.form.is_valid():
			action = self.form.cleaned_data['action']
			files = self.form.cleaned_data['files'].split(', ')
			pattern = self.form.cleaned_data['pattern']
			count = self.form.cleaned_data['count']
			word_regexp = self.form.cleaned_data['word_regexp']
			invert = self.form.cleaned_data['invert']
			line_number = self.form.cleaned_data['line_number']
			recursive = self.form.cleaned_data['recursive']

			if recursive:
				directory = self.form.cleaned_data['files']
				files = grep.recursive_files_search(directory)


			func = grep.find_lines_with_match
			if invert:
				func = grep.find_lines_with_non_match
			
			if action == 'Lines':
				files_with_lines = grep.get_files_with_lines(files, pattern,
							func, word_regexp, line_number=line_number)
				return render_to_response('grep_result_lines.html',
					 {'files_with_lines': files_with_lines})

			elif action == 'Files':
				files_with_match = []
				if not count:
					files_with_match = grep.get_files_with_match(files,
							pattern, func, word_regexp)
				files_with_counts = grep.get_files_with_lines(files,
						pattern, grep.get_count_of_matches, word_regexp)

				return render_to_response('grep_result_files.html',
					 {'files_with_counts': files_with_counts, 
					  'files_with_match': files_with_match,
					  'count': count})

		else:
			return super(GrepRequestCreate, self).get(request, *args, **kwargs)
