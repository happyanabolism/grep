# -*- coding: utf-8 -*-

from django import forms
from django.core.exceptions import ValidationError, ObjectDoesNotExist

from models import GrepRequest, ACTIONS


class GrepRequestCreateForm(forms.ModelForm):
	class Meta:
		model = GrepRequest
		fields = ['action', 'files', 'pattern', 'count', 'invert',
				  'word_regexp', 'line_number', 'recursive']

	action = forms.ChoiceField(label='Action',
							   choices=ACTIONS,
							   widget = forms.RadioSelect,
							   required=True)
	files = forms.CharField(widget=forms.Textarea(attrs={"rows": 5, "cols": 70}),
							required=True)

