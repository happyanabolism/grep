# -*- coding: utf-8 -*-

from django.db import models


ACTIONS = (
	(u'Lines', u'Lines'),
	(u'Files', u'Files')
)

class GrepRequest(models.Model):
	files = models.TextField()
	action = models.CharField(max_length=10, choices=ACTIONS)
	pattern = models.CharField(max_length=40)
	count = models.BooleanField()
	invert = models.BooleanField()
	word_regexp = models.BooleanField()
	line_number = models.BooleanField()
	recursive = models.BooleanField()
